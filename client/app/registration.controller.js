(function () { //IIFE used -> client side 

    angular.module("RegApp").controller("RegistrationCtrl", RegistrationCtrl); // i am naming this controlling as registration control and injected an angular component called $http

    // best practise, each controller is given a angular service eg.$http,etc

    RegistrationCtrl.$inject = ["$http"]

    function RegistrationCtrl($http) {
        var self = this; // vm, this for the controller to point to this variable. 

        self.isMinor = false;
        console.log(self.isMinor);
        self.students = [];
        console.log("--->" + self.students);
        self.formShow = false;
        self.entryHide = false;

        self.checkAge = function(){
                    self.formShow = true;
                    self.entryHide = true;
            }
            
        self.user = {
            email: "",
            password: "",
            fullname: "",
            gender: "",
            dateofbirth: "",
            address: "",
            country: "",
            contactnumber: ""
        };


        self.initForm = function(){   //this is refresh the page
            console.log("page refresh")
        };
        self.initForm();

        self.inituser = function(){
            console.log("Getting users...")
            $http.get("/users")
            .then(function (result) {
                self.students = result.data;
                console.log("Student data received is:" + result.data)
                })
                .catch(function(e) {
                    console.log(e);
                });
            }; 


        self.inituser();

        self.below18 = function() {
            var isMinor = false;
            var birthDate = new Date(self.user.dateofbirth);
            var today = new Date();
            // sort out the year difference first          
            var age = today.getFullYear() - birthDate.getFullYear();
            // if the remaining time is less than 12 months
            // e.g. Sep2017 - Dec1996 OR 4Sep2017 - 16Sep1996
            var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            if (age < 18) {
                isMinor = true;
            }return isMinor
        };
        (console.log("--->" + self.below18()));

        self.displayUser = {
            email: "",
            password: "",
            fullname: "",
            gender: "",
            dateofbirth: "",
            address: "",
            country: "",
            contactnumber: ""
        };

        self.onlyFemale = function(){
            return self.user.gender == "M"
        }

        //self.over18 = function(){}
        
        self.refreshScreen = function() {
            console.log("Refreshing screen...");
            window.location.reload(true);
            self.initForm();
        };

        self.checkFormValidity = function(){
            if (registrationForm.$valid == true){
                return true;
            }
        }


        self.registerUser = function (){
            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.fullname);
            console.log(self.user.gender);
            console.log(self.user.dateofbirth);
            console.log(self.user.address);
            console.log(self.user.country);
            console.log(self.user.contactnumber);
            $http.post("/users", self.user) // promise here
                .then(function (result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    self.displayUser.fullname = result.data.fullname;
                    self.displayUser.gender = result.data.gender;
                    self.displayUser.dateofbirth = result.data.dateofbirth;
                    self.displayUser.address = result.data.address;
                    self.displayUser.country = result.data.country;
                    self.displayUser.contactnumber = result.data.contactnumber;
                })
                .catch(function (e) {
                    console.log(e);
                });
            };
        }
    
})();

