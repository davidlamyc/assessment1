console.log("Starting...");
var express = require("express"); // "require" used -> server side , this code takes in express, a middleware as it interfaces the client side and the server
var bodyParser = require("body-parser"); //body parser will enable express to interpret the body of clientside index.html as a json
var app = express();

app.use(bodyParser.urlencoded({extended: false})); // code 6&7 registers bodyParser as a plugin
app.use(bodyParser.json());


console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/")); // tells express where is the ux ui directory and loads the relevant files
// once we are angular, the client file is no longer just static but has dynamic components. 

// app.use('/bower_components', express.static(__dirname + "/../client/bower_components")); 

app.post("/users", function(req, res){
    var user = req.body;
    console.log("Received user object: " + req.body);
    console.log("email > " + user.email);
    console.log("password > " + user.password);
    console.log("fullname > " + user.fullname);
    console.log("gender > " + user.gender);
    console.log("date of birth > " + user.dateofbirth);
    console.log("address > " + user.address);
    console.log("country > " + user.country);
    console.log("contact number > " + user.contactnumber);
    res.status(200).json(user);
});


app.use(function(req, res) {
    res.send("<h1>404 Error. Please try another door.</h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});